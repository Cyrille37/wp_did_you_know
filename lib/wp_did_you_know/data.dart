library;

import 'package:mysql_utils/mysql_utils.dart';
import 'package:mysql_client/exception.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert';

abstract class Data {
  void create();
  void close();
  void store(String item);
}

class MysqlData extends Data {
  final String table = 'anecdotes';
  late var _db;
  final List<Future<void>> _tasks = <Future<void>>[];

  MysqlData(Map config) {
    _db = MysqlUtils(
      settings: {
        'host': config['host'],
        'port': config['port'],
        'user': config['user'],
        'password': config['password'],
        'db': config['db'],
        'maxConnections': 10,
        'secure': false,
        'prefix': '',
        'pool': false,
        'collation': 'utf8mb4_general_ci',
      },
      errorLog: (error) {
        //print('Error: $error');
        throw Exception(error);
      },
      sqlLog: (sql) {
        print('SQL: $sql');
      },
      connectInit: (db1) async {
        print('Db connected.');
      },
    );
    create();
  }

  @override
  void create() {
    () async {
      await _db.query('''
      CREATE TABLE IF NOT EXISTS $table (
        id INT NOT NULL AUTO_INCREMENT,
        hash CHAR(40) NOT NULL,
        text TEXT NOT NULL,
        PRIMARY KEY (id),
        UNIQUE(hash),
        FULLTEXT(text)
      ) ENGINE=INNODB
    ''');
    }();
  }

  @override
  void store(String item) {
    var bytes = utf8.encode(item);
    _tasks.add(_store(item, sha1.convert(bytes).toString()));
  }

  Future<void> _store(String item, String digest) async {
    try {
      //print('Inserting...');
      await _db.insert(
        table: table,
        debug: false,
        insertData: {
          'hash': digest,
          'text': item,
        },
      );
      //print('Inserted.');
    } catch (e) {
      if (e
          .toString()
          .contains('MySQLServerException [1062]: Duplicate entry')) {
        //print('Already.');
        return;
      }
      print(e);
      rethrow;
    }
  }

  @override
  void close() {
    //print('Closing...');
    () async {
      print('Storing count: ${_tasks.length}');
      await Future.wait(_tasks);
      await _db.close();
      print('Done & closed.');
    }();
  }
}
