///
/// Retrouve les anecdotes sur l'acceuil
///

library;

import 'package:http/http.dart' as http;
import 'package:html/dom.dart';
import 'package:html/dom_parsing.dart';
import 'package:html/parser.dart';
import 'package:injector/injector.dart';
import 'package:wp_did_you_know/wp_did_you_know/data.dart';

Future wpDidYouKnowDay() async {
  Data? data = Injector.appInstance.get<Data?>();

  final uri = Uri.https('fr.wikipedia.org',
      '/wiki/Wikipédia:Le saviez-vous ?/Anecdotes sur l\'accueil');

  print(uri.toString());
  final response = await http.get(uri);
  if (response.statusCode != 200) {
    throw Exception('Failed to get page ${response.statusCode}');
  }

  var doc = parse(response.body).documentElement!;
  // <div id="mw-content-text" class="mw-body-content"><div class="mw-content-ltr mw-parser-output" lang="fr" dir="ltr"><ul>
  var elements = doc.querySelectorAll('div[id="mw-content-text"] ul li');

  print('Wikipedia "Le Saviez-vous" ?');
  print('elements count: ${elements.length}\n');

  var visitor = _Visitor();

  Future.wait(elements.map((node) => visitor.extract(node)));
  for (String item in visitor.getResults()) {
    data != null ? data.store(item) : print('$item\n');
  }
}

// Note: this example visitor doesn't handle things like printing attributes and
// such.
class _Visitor extends TreeVisitor {
  String result = '';
  String indent = '';
  List<String> results = <String>[];

  List<String> getResults() {
    return results;
  }

  String getResult() {
    return result;
  }

  Future<void> extract(Node node) async {
    //indent = '';
    result = "";
    super.visit(node);
    results.add(result);
  }

  @override
  void visitText(Text node) {
    if (node.data.trim().isNotEmpty) {
      //print('$indent${node.data.trim()}');
    }
    result += node.data;
  }

  @override
  void visitElement(Element node) {
    if (node.localName == 'figure') return;

    if (isVoidElement(node.localName)) {
      //print('$indent<${node.localName}/>');
    } else {
      //print('$indent<${node.localName}>');
      //print('$indent<${node.attributes}>');
      switch (node.localName) {
        case 'a':
          //print('$indent${node.attributes["href"]}');
          var href = node.attributes["href"];
          if (href!.startsWith('/wiki/')) {
            result += '<a href="$href">';
          }
          break;
        case 'li':
          break;
        default:
          result += '<${node.localName!}>';
      }
      indent += '  ';
      visitChildren(node);
      indent = indent.substring(0, indent.length - 2);
      //print('$indent</${node.localName}>');
      switch (node.localName) {
        case 'li':
          break;
        default:
          result += '</${node.localName!}>';
      }
    }
  }

  @override
  void visitChildren(Node node) {
    for (var child in node.nodes) {
      visit(child);
    }
  }
}
