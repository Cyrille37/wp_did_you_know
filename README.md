# wp-did-you-know

![Hélène Rochette, Les fluides, 2007][visuel]

Premiers pas avec [dart](https://dart.dev/)
avec un aspirateur de "[Wikipédia:Le saviez-vous ?](https://fr.wikipedia.org/wiki/Wikip%C3%A9dia:Le_saviez-vous_%3F)".

Utilisation des packages:
- [http](https://pub.dev/packages/http) pour récupérer les pages html ;
- [html](https://pub.dev/packages/html) pour lire ces pages et en extraite le substantiel jus ;
- [args](https://pub.dev/packages/args) pour lire les options de la ligne de commande ;
- [yaml](https://pub.dev/packages/yaml) pour le fichier de configuration ;
- [dotenv](https://pub.dev/packages/dotenv) pour surchager les valeurs du fichier de configuration ;
- [injector](https://pub.dev/packages/injector) pour découpler les dépendances d'instances d'objets.
- [mysql_utils](https://pub.dev/packages/mysql_utils) pour stocker les anecdotes récupérées dans un serveur Mysql
- [crypto](https://pub.dev/packages/crypto) pour calculer un digest (sha) utiliser pour assurer des anecdotes uniques ;

Le programe principal est dans `./bin` et ses éléments de code dans `./lib`.

Il y a plein d'asynchronisme mais c'est pour me faire la main, car l'exécution n'utilise qu'un seul et unique thread. Pour du multithreading il faudrait utiliser [`Isolate`](https://dart.dev/language/concurrency#isolates).

Sur le Linux le binaire compilé fait 7,4Mo (7691552 bytes). En séparant le info de debug dans un autre fichier c'est 6,4Mo (debug-info 729k).

## En vrac

A `dart` command-line application with an entrypoint in `bin/`, library code
in `lib/`, and example unit test in `test/`.

```
$ dart create wp-did-you-know
$ cd create wp-did-you-know
$ dart pub add args
$ dart pub add http
$ dart pub add html
$ dart run
$ dart compile exe bin/wp_did_you_know.dart
$ ./bin/wp-did-you-know.exe
```


## Crédits

Le visuel en début de fichier en CC By 2.0 [Hélène Rochette, Les fluides, 2007](https://commons.wikimedia.org/wiki/File:H%C3%A9l%C3%A8ne_Rochette,_Les_fluides,_2007._(19355000142).jpg). Pourquoi ce choix ? Cette image est dans les premières réponses de https://commons.wikimedia.org à la question "Le saviers-vous ?" ;-)

Tous les trucs de `dart` sont issus de https://dart.dev et https://pub.dev.

[visuel]: media/Hélène%20Rochette%20-%20Les_fluides%20-%202007.jpg "Hélène Rochette, Les fluides, 2007"
