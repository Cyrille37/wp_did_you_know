import 'dart:io';
import 'package:args/args.dart';
import 'package:injector/injector.dart';
import 'package:wp_did_you_know/wp_did_you_know/day.dart';
import 'package:wp_did_you_know/wp_did_you_know/archives.dart';
import 'package:wp_did_you_know/wp_did_you_know/config.dart';
import 'package:wp_did_you_know/wp_did_you_know/data.dart';

/// Usage:
///
/// $ dart run bin/wp_did_you_know.dart day
/// $ dart run bin/wp_did_you_know.dart --year=2023 archives

void main(List<String> arguments) async {
  // "dart:io" library defines a top-level property "exitCode".
  exitCode = 0; // Presume success

  // https://pub.dev/packages/args
  final parser = ArgParser()
    ..addOption('year', abbr: 'y')
    ..addFlag('store', abbr: 's', defaultsTo: false);

  ArgResults argResults = parser.parse(arguments);

  if (argResults.rest.length != 1) {
    print('''
    Usage:
      For day's tips:
        Just display : wp_did_you_know.dart day
        Store in Mysql: wp_did_you_know.dart day --store
      For an year archives tips:
        Just display : wp_did_you_know.dart archives --year=2023
        Store in Mysql: wp_did_you_know.dart archives --year=2023 --store
    ''');
    exitCode = 1;
    return;
  }

  final injector = Injector.appInstance;
  injector.registerSingleton<Config>(() {
    return YamlConfig('wp_did_you_know.conf');
  });
  injector.registerSingleton<Data?>(() {
    final config = injector.get<Config>();
    if (argResults['store']) {
      return MysqlData(config['database'] as Map);
    }
    return null;
  });

  switch (argResults.rest[0]) {
    case 'archives':
      int year = 0;
      try {
        year = int.parse(argResults['year']);
      } catch (ex) {
        print('Error: "Year" is mandatory for "archives".');
        exitCode = 2;
        return;
      }
      await wpDidYouKnowArchives(year);
      _closingStuff();
      break;

    case 'day':
      await wpDidYouKnowDay();
      _closingStuff();
      break;

    default:
      exitCode = 2;
      throw UnimplementedError('bouh!');
  }
}

void _closingStuff() {
  Data? data = Injector.appInstance.get<Data?>();
  if (data != null) data.close();
}
